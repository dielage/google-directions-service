class GoogleDirection {

  constructor(googleMapsClient) {
    this.googleMapsClient = googleMapsClient;
  }


  getDirections(origin, destination, mode) {
    return new Promise((resolve, reject) => {
      this.googleMapsClient.directions({
        'origin': origin,
        'destination': destination,
        'mode': mode,
      }).asPromise()
        .then((response) => {
          let data = response.json
          resolve(data)
        }).catch((err) => {
        reject(err)
      })
    })
  }
}

module.exports = GoogleDirection

