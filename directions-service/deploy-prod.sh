#!/usr/bin/env bash

# Deploy this service on google cloud run

export SERVICE_NAME="directions-service"
export PROJECT_NAME="<project-name>"

docker build . --tag gcr.io/$PROJECT_NAME/$SERVICE_NAME --no-cache

docker push gcr.io/$PROJECT_NAME/$SERVICE_NAME:latest

gcloud run deploy $SERVICE_NAME --image gcr.io/$PROJECT_NAME/$SERVICE_NAME --platform managed --region=europe-west3

