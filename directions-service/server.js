'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const googleMaps = require('@google/maps');
const GoogleMapsDirections = require('./directions');
const cors = require('cors');

// TODO rewrite to ES6

// App
const app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());


// Constants
const PORT = 8080;
const HOST = '0.0.0.0';


// Paste in your google maps api key
const googleMapsClient = googleMaps.createClient({
  key: <googleMapsKey>,
  Promise: Promise
});


app.get('/map-direction', async (req, res) => {
  const origin = req.query.origin;
  const destination = req.query.destination;
  const mode = req.query.mode;
  const Directions = new GoogleMapsDirections(googleMapsClient)

  Directions.getDirections(origin, destination, mode).then((result) => {
    res.send(result)
  }).catch((err) => {
    concole.log(err);
  })
});


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

//curl -d '{"origin" :"Berlin Hauptbahnhof", "destination": "Berlin Lichtenrade", "mode": "driving"}' -H "Content-Type: application/json" http://localhost:8080/directions --verbose
//curl -X GET 'http://localhost:8080/map-direction?origin=Berlin&destination=Hamburg&mode=driving'
